package main

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/boltdb/bolt"
)

var doImport = flag.Bool("import", false, "Imports data from the 2 gists. Should only be run once.")
var restServer = flag.Bool("server", false, "Starts the server and exposes the endpoint.")

type Category string

type Employee struct {
	Name       string
	Categories []Category `json:"interests"`
}

type Product struct {
	Name       string
	Categories []Category
}

type EmployeesResponse struct {
	Collection []Employee
}

type ProductResponse struct {
	Collection []Product
}

func main() {
	flag.Parse()
	if *doImport {
		employees, err := fetchEmployees()
		if err != nil {
			log.Fatalf("errored while importing employees: %s", err.Error())
		}

		products, err := fetchProducts()
		if err != nil {
			log.Fatalf("errored while importing products: %s", err.Error())
		}

		db, err := bolt.Open("database", 0600, &bolt.Options{Timeout: 1 * time.Second})
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()

		if false {
			if err := db.Update(func(tx *bolt.Tx) error {
				var (
					employeesBucket *bolt.Bucket
					productsBucket  *bolt.Bucket
					err             error
				)

				_ = tx.DeleteBucket([]byte("employees"))
				_ = tx.DeleteBucket([]byte("products"))

				employeesBucket, err = tx.CreateBucket([]byte("employees"))
				if err != nil {
					return fmt.Errorf("create bucket: %s", err)
				}

				productsBucket, err = tx.CreateBucket([]byte("products"))
				if err != nil {
					return fmt.Errorf("create bucket: %s", err)
				}

				var buf bytes.Buffer

				for _, employee := range employees {
					enc := gob.NewEncoder(&buf)
					err := enc.Encode(employee)
					if err != nil {
						return err
					}
					key := employee.GetKey()
					if err = employeesBucket.Put(key, buf.Bytes()); err != nil {
						return err
					}
				}

				for _, product := range products {
					enc := gob.NewEncoder(&buf)
					err := enc.Encode(product)
					if err != nil {
						return err
					}
					if err = productsBucket.Put(product.GetKey(), buf.Bytes()); err != nil {
						return err
					}
				}

				return nil
			}); err != nil {
				log.Fatalf("errored doing db maintenance work: %s", err.Error())
			}
		}

		// test
		if err := db.View(func(tx *bolt.Tx) error {
			employeesBucket := tx.Bucket([]byte("employees"))
			c := employeesBucket.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {
				buf2 := bytes.NewBuffer(v)
				dec := gob.NewDecoder(buf2)
				var employee Employee
				if err := dec.Decode(&employee); err != nil {
					return err
				}
				// showing same first employee for some reason
				fmt.Printf("key=%s\nvalue: %v\n", k, employee)
			}
			return nil
		}); err != nil {
			log.Fatalf("errored viewing db: %s", err.Error())
		}

		return
	}
}

func (e Employee) GetKey() []byte {
	return []byte(e.Name)
}

func (e *Employee) UnmarshalBinary(data []byte) (err error) {
	err = json.Unmarshal(data, e)
	if err != nil {
		err = fmt.Errorf("errored while unmarshaling: %s", err.Error())
	}
	return
}

func (e Employee) MarshalBinary() (data []byte, err error) {
	b, err := json.Marshal(e)
	if err != nil {
		return
	}
	return b, nil
}

func (p Product) GetKey() []byte {
	return []byte(p.Name)
}

func (p *Product) UnmarshalBinary(data []byte) (err error) {
	err = json.Unmarshal(data, p)
	if err != nil {
		err = fmt.Errorf("errored while unmarshaling: %s", err.Error())
	}
	return
}

func (p Product) MarshalBinary() (data []byte, err error) {
	b, err := json.Marshal(p)
	if err != nil {
		return
	}
	return b, nil
}

func fetchEmployees() (employees []Employee, err error) {
	employeesBody, err := getContent("https://gist.githubusercontent.com/hcliff/5bf865e0c5e849c726a2478908860303/raw/2ece0a10c62ef52fc9edd2fac1b18a81a24fa13d/employees.json")
	if err != nil {
		return
	}
	employees = make([]Employee, 0)
	err = json.Unmarshal(employeesBody, &employees)
	return
}

func fetchProducts() (products []Product, err error) {
	productsBody, err := getContent("https://gist.githubusercontent.com/hcliff/5bf865e0c5e849c726a2478908860303/raw/2ece0a10c62ef52fc9edd2fac1b18a81a24fa13d/gifts.json")
	if err != nil {
		return
	}
	products = make([]Product, 0)
	err = json.Unmarshal(productsBody, &products)
	return
}

func getContent(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Read body: %v", err)
	}

	return data, nil
}
